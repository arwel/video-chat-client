import typescript from 'rollup-plugin-typescript2';

export default {
    input: 'src/ts/main.ts',
    output: {
        file: 'dist/js/index.js',
        format: 'iife'
    },
    plugins: [
        typescript({
            tsconfig: 'tsconfig.json'
        })
    ]
};
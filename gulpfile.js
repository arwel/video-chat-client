const Fiber = require('fibers');
const {task, watch, parallel, src, dest} = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
sass.compiler = require('sass');

const SASS_SRC = './src/sass/main.sass';
const SASS_DEST = './dist/css';
const SASS_CONFIG = {
    fiber: Fiber
};
const AUTOPERFIXER_CONFIG = {
    cascade: false,
    grid: "autoplace"
};


const compileDevSass = () => src(SASS_SRC)
    .pipe(sourcemaps.init())
    .pipe(sass(SASS_CONFIG).on('error', sass.logError))
    .pipe(autoprefixer(AUTOPERFIXER_CONFIG))
    .pipe(sourcemaps.write('.'))
    .pipe(dest(SASS_DEST));

const compileBuildSass = () => src(SASS_SRC)
    .pipe(sass(SASS_CONFIG).on('error', sass.logError))
    .pipe(autoprefixer(AUTOPERFIXER_CONFIG))
    .pipe(cleanCSS({compatibility: 'ie11'}))
    .pipe(dest(SASS_DEST));

task('watch:sass', () => watch(SASS_SRC, parallel(compileDevSass)));
task('build:sass', compileBuildSass);
